from vinput import VirtualInput
import aiohttp
from aiohttp import web
import msgpack
from evdev import ecodes as e


_cached_html = None
vinput = VirtualInput()


async def index(request):
#    global _cached_html

#    if not _cached_html:
    with open('ui/dist/index.html', 'r') as html:
        _cached_html = html.read()
        return web.Response(text=_cached_html, content_type='text/html')


async def echo_ws(request):
    global vinput
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.ERROR:
            print("Connection error: " + ws.exception())
        elif msg.type == aiohttp.WSMsgType.CLOSE:
            await ws.close()
        else:
            vinput_action = msgpack.unpackb(msg.data, encoding='utf-8')

            try:
                if vinput_action['type'] == 'type-string':
                    s = vinput_action['string']
                    print("typing string: " + s)
                    await vinput.string_type(s)

                elif vinput_action['type'] == 'mouse-move':
                    x = vinput_action['dx']
                    y = vinput_action['dy']

                    await vinput.mouse_move_rel(x, y)
                elif vinput_action['type'] == 'mouse-click':
                    await vinput.key_type([e.ecodes['BTN_' +
                                                    vinput_action['button']]])
            except:
                vinput.close()
                vinput = VirtualInput()
                raise

    print('connection closed')

    return ws


def make_app():
    app = web.Application()
    app.routes.get('/', index)
    app.routes.get('/vinputws', echo_ws)
    app.routes.static('/static', 'ui/dist')

    return app

if __name__ == '__main__':
    web.run_app(make_app())
