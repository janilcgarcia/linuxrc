import msgpack
import evdev
from evdev import ecodes as e
import asyncio


class VirtualInput(object):
    def __init__(self):
        cap = {
            e.EV_REL: [e.REL_X, e.REL_Y],
            e.EV_KEY: list(e.keys.keys())
        }

        self._uinput = evdev.UInput(cap, name='linuxrc', version=0x0,
                                    vendor=0xff, product=0x01,
                                    bustype=e.BUS_USB)

        self._lock = asyncio.Lock()

    async def close(self):
        self._uinput.close()

    async def key_press(self, keycodes):
        for keycode in keycodes:
            self._uinput.write(e.EV_KEY, keycode, 1)
        self._uinput.syn()

    async def key_release(self, keycodes):
        for keycode in keycodes:
            self._uinput.write(e.EV_KEY, keycode, 0)
        self._uinput.syn()

    async def key_type(self, keycodes):
        await self.key_press(keycodes)
        await self.key_release(keycodes)

    async def mouse_move_rel(self, x, y):
        self._uinput.write(e.EV_REL, e.REL_X, x)
        self._uinput.write(e.EV_REL, e.REL_Y, y)
        self._uinput.syn()

    async def string_type(self, s):
        for c in s:
            await self._char_to_key_type(c)

    async def _char_to_key_type(self, c):
        keys = []

        if c.isupper():
            keys.append(e.KEY_LEFTSHIFT)

        if c.isalnum():
            keys.append(e.ecodes['KEY_' + c.upper()])
        elif c == '/':
            keys.append(e.KEY_SLASH)
        elif c == '\\':
            keys.append(e.KEY_BACKSLASH)
        elif c == '?':
            keys.append(e.KEY_QUESTION)
        elif c == ';':
            keys.append(e.KEY_SEMICOLON)
        elif c == ':':
            keys.append(e.KEY_LEFTSHIFT)
            keys.append(e.KEY_SEMICOLON)
        elif c == '.':
            keys.append(e.KEY_DOT)
        elif c == ' ':
            keys.append(e.KEY_SPACE)

        await self.key_type(keys)
