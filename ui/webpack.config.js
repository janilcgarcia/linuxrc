const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['bootstrap-loader', './src/index.js'],
    output: {
	path: path.resolve(__dirname, 'dist'),
	filename: '[name].js',
	publicPath: '/static/'
    },
    module: {
	rules: [
	    {
		test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
		loader: 'imports-loader?jQuery=jquery'
	    },
	    {
		test: /\.(woff2?|svg)$/,
		loader: 'url-loader?limit=10000'
	    },
	    {
		test: /\.(ttf|eot)$/,
		loader: 'file-loader'
	    }
	]
    },
    plugins: [
	new HtmlWebpackPlugin({
	    template: path.resolve(__dirname, 'index.html')
	})
    ]
};
