var $ = require('jquery');
var msgpack = require('msgpack-lite');

require('!style-loader!css-loader!sass-loader!./styles/style.sass');

window.jQuery = $;

$(document).ready(function () {
    var formSetEnable = function (enabled) {
	$("form").find("*").attr("disabled", !enabled);
    };

    formSetEnable(false);

    var socket = new WebSocket("ws://" + window.location.host + "/vinputws");

    socket.onopen = function () {
	formSetEnable(true);
    };

    socket.onerror = function (error) {
	console.log("Error: " + error.message);
    };

    socket.onclose = function () {
	formSetEnable(false);
    };

    socket.onmessage = function (msg) {
	console.log("Got message: " + msg.data);
    };


    var textInput = $("#text-input");
    var sendInput = $("#send-input");
    var form = $("form");

    var canvas = $("canvas");
    var body = $(document.body);

    var autoCanvasSize = function autoCanvasSize() {
	if (body.width() > 480) {
	    canvas.width(480);
	} else {
	    canvas.width(body.width() - 25);
	}

	canvas.height(9/16 * canvas.width())
    };

    autoCanvasSize();

    $(window).on('resize', autoCanvasSize);;

    form.on('submit', function (ev) {
	ev.preventDefault();
	if (socket.readyState !== WebSocket.OPEN) {
	    formSetEnable(false);
	    alert("Socket not open!");
	}

	var text = textInput.val();
	socket.send(msgpack.encode({
	    "type": "type-string",
	    "string": text
	}));
    });

    if (!Date.now) {
	Date.now = function () {
	    return new Date().getTime();
	}
    }

    var mouseHandler = {
	prevPosition: {
	    x: 0,
	    y: 0
	},

	enter: function (x, y, touch) {
	    if (touch === null) {
		touch = 0;
	    }
	    
	    this.prevPosition = {x: x, y: y};
	},

	move: function (x, y) {
	    var dx = x - this.prevPosition.x;
	    var dy = y - this.prevPosition.y;

	    this.prevPosition = {
		x: x,
		y: y
	    };

	    socket.send(msgpack.encode({
		"type": "mouse-move",
		"dx": dx,
		"dy": dy
	    }));
	},

	click: function (event) {
	    var button;

	    if (event.button == 0)
		button = "LEFT";
	    else if (event.button == 1)
		button = "MIDDLE";
	    else
		button = "RIGHT";

	    socket.send(msgpack.encode({
		"type": "mouse-click",
		"button": button
	    }));
	}
    };

    function bind(obj, method) {
	return function () {
	    obj[method].apply(obj, arguments);
	};
    }

    canvas.on('mouseenter', function (event) {
	mouseHandler.enter(event.screenX, event.screenY);
    });
    
    canvas.on('mousemove', function (event) {
	mouseHandler.move(event.screenX, event.screenY);
    });
    
    canvas.on('click', bind(mouseHandler, 'click'));
    canvas.on('touchstart', function (event) {
	var touch = event.changedTouches[0];
	mouseHandler.enter(touch.screenX, touch.screenY);
    });
    canvas.on('touchmove', function (event) {
	var touch = event.changedTouches[0];

	mouseHandler.move(touch.screenX, touch.screenY);
    });
});
